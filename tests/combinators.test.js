import { expect, jest, test } from "@jest/globals";
import ObjectFilter from "../src/ObjectFilter.js";


const testData = [
	{ foo: "bar", fizz: "buzz", index: 0 },
	{ foo: "baz", fizz: "burr", index: 1 },
	{ foo: "baz", fizz: "buzz", index: 2 },
	{ foo: "bar", fizz: "burr", index: 3 },
	{ foo: "bar", fizz: "buzz", index: 4 },
	{ foo: "baz", fizz: "burr", index: 5 },
	{ foo: "bar", fizz: "burr", index: 6 },
	{ foo: "baz", fizz: "burr", index: 7 }
];

describe( "And combinator", andTestCases );
describe( "Or combinator", orTestCases );

function andTestCases(){
	let Filter = new ObjectFilter({ and: [
		{ property: "foo", eq: "bar" },
		{ property: "fizz", eq: "buzz" }
	] });
	let result = Filter.apply( testData );

	test( "returns correct number of elements", () => expect( result.length ).toBe( 2 ) );

	test( "returns items in same order", () => {
		expect( result[ 0 ].index ).toBe( 0 );
		expect( result[ 1 ].index ).toBe( 4 );
	});

	test( "only returns values that match filter criteria", () => {
		expect( JSON.stringify( result[ 0 ] ) ).toBe( JSON.stringify( testData[ 0 ] ) );
		expect( JSON.stringify( result[ 1 ] ) ).toBe( JSON.stringify( testData[ 4 ] ) );
	});

}

function orTestCases(){
	let Filter = new ObjectFilter({
		or: [
			{ property: "foo", eq: "bar" },
			{ property: "fizz", eq: "buzz" }
		]
	});
	let result = Filter.apply( testData );

	test( "returns correct number of elements", () => expect( result.length ).toBe( 5 ) );

	test( "returns items in same order", () => {
		expect( result[ 0 ].index ).toBe( 0 );
		expect( result[ 1 ].index ).toBe( 2 );
		expect( result[ 2 ].index ).toBe( 3 );
		expect( result[ 3 ].index ).toBe( 4 );
		expect( result[ 4 ].index ).toBe( 6 );
	});

	test( "only returns values that match filter criteria", () => {
		expect( JSON.stringify( result[ 0 ] ) ).toBe( JSON.stringify( testData[ 0 ] ) );
		expect( JSON.stringify( result[ 1 ] ) ).toBe( JSON.stringify( testData[ 2 ] ) );
		expect( JSON.stringify( result[ 2 ] ) ).toBe( JSON.stringify( testData[ 3 ] ) );
		expect( JSON.stringify( result[ 3 ] ) ).toBe( JSON.stringify( testData[ 4 ] ) );
		expect( JSON.stringify( result[ 4 ] ) ).toBe( JSON.stringify( testData[ 6 ] ) );
	});
}