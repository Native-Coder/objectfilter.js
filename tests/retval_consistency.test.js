import { expect, jest, test } from "@jest/globals";
import ObjectFilter from "../src/ObjectFilter.js";

describe( "Consistently returns data", dataReturnConsistency );

function dataReturnConsistency(){
	const testData = [
		{ foo: "bar", index: 0 },
		{ foo: "baz", index: 1 },
		{ foo: "fizz", index: 2 }
	];
	const Filter = new ObjectFilter();
	const result = Filter.apply( testData );

	test( "returns correct number of elements", () => expect( result.length ).toBe( 3 ) );

	test( "returns items in same order", () => {
		expect( result[ 0 ].index ).toBe( 0 );
		expect( result[ 1 ].index ).toBe( 1 );
		expect( result[ 2 ].index ).toBe( 2 );
	});

	test( "only returns values that match filter criteria", () => {
		expect( JSON.stringify( result[ 0 ] ) ).toBe( JSON.stringify( testData[ 0 ] ) );
		expect( JSON.stringify( result[ 1 ] ) ).toBe( JSON.stringify( testData[ 1 ] ) );
		expect( JSON.stringify( result[ 2 ] ) ).toBe( JSON.stringify( testData[ 2 ] ) );
	});
}