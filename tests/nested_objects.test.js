import { expect, jest, test } from "@jest/globals";
import ObjectFilter from "../src/ObjectFilter.js";


describe( "Single level object nesting", singleLevelTestCases );
describe( "Multi-level object nesting", multiLevelTestCases );
describe( "Skip property nesting", literalPropKey );

function singleLevelTestCases(){

	const testData = [
		{ foo: { bar: "baz" }, index: 0 },
		{ foo: { bar: "buzz" }, index: 1 },
		{ fizz: { buzz: "baz" }, index: 2 },
		{ foo: { bar: "baz" }, index: 3 } 
	];

	const filter = new ObjectFilter({ property: "foo.bar", eq: "baz" });
	const result = filter.apply( testData );

	test( "returns correct number of elements", () => expect( result.length ).toBe( 2 ) );

	test( "returns items in same order", () => {
		expect( result[ 0 ].index ).toBe( 0 );
		expect( result[ 1 ].index ).toBe( 3 );
	});

	test( "only returns values that match filter criteria", () => {
		expect( JSON.stringify( result[ 0 ] ) ).toBe( JSON.stringify( testData[ 0 ] ) );
		expect( JSON.stringify( result[ 1 ] ) ).toBe( JSON.stringify( testData[ 3 ] ) );
	});
}

function multiLevelTestCases(){

	const testData = [
		{ foo: { bar: { baz: "hello_world" } },index: 0 },
		{ foo: { bar: { baz: "hello_worlds" } },index: 1 },
		{ foo: { bar: { baz: "no!" } },index: 2 },
		{ foo: { bar: { baz: "hello_world" } },index: 3 },
		{ foo: { bar: { baz: "yes!" } },index: 4 },
		{ foo: { bar: { baz: "hello_world" } },index: 5 },
	];

	const filter = new ObjectFilter({ property: "foo.bar.baz", eq: "hello_world" });
	const result = filter.apply( testData );

	test( "returns correct number of elements", () => expect( result.length ).toBe( 3 ) );

	test( "returns items in same order", () => {
		expect( result[ 0 ].index ).toBe( 0 );
		expect( result[ 1 ].index ).toBe( 3 );
		expect( result[ 2 ].index ).toBe( 5 );
	});

	test( "only returns values that match filter criteria", () => {
		expect( JSON.stringify( result[ 0 ] ) ).toBe( JSON.stringify( testData[ 0 ] ) );
		expect( JSON.stringify( result[ 1 ] ) ).toBe( JSON.stringify( testData[ 3 ] ) );
		expect( JSON.stringify( result[ 2 ] ) ).toBe( JSON.stringify( testData[ 5 ] ) );
	});
}

function literalPropKey() {
	const testData = [
		{ "foo.bar": "baz", fizz: "buzz", index: 0 },
		{ "foo.bar": "bar", fizz: "buzz", index: 1 }
	];

	const Filter = new ObjectFilter({ property: "foo.bar", eq: "baz", literalPropKey: true });
	const result = Filter.apply( testData );

	test( "literalPropKey allows the use of periods in property keys", () => {
		expect( JSON.stringify( result[ 0 ] ) ).toBe( JSON.stringify( testData[ 0 ] ) );
	});
}