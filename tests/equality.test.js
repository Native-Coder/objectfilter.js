import { expect, jest, test } from "@jest/globals";
import ObjectFilter from "../src/ObjectFilter.js";

const testData = [
	{ booleanValue: true, index: 0 },
	{ booleanValue: "true", index: 1 },
	{ booleanValue: "1", index: 2 },
	{ booleanValue: 1, index: 3 },
	{ booleanValue: true, index: 4 }
];

describe( "Equality", eqTestCases );
describe( "Exact equality", eeqTestCases );

function eqTestCases() {
	const Filter = new ObjectFilter({ property: "booleanValue", eq: true });
	const result = Filter.apply( testData );

	test( "returns correct number of elements", () => expect( result.length ).toBe( 4 ) );

	test( "returns items in same order", () => {
		expect( result[ 0 ].index ).toBe( 0 );
		expect( result[ 1 ].index ).toBe( 2 );
		expect( result[ 2 ].index ).toBe( 3 );
		expect( result[ 3 ].index ).toBe( 4 );
	});

	test( "only returns values that match filter criteria", () => {
		expect( result[ 0 ].booleanValue ).toBe( true );
		expect( result[ 1 ].booleanValue ).toBe( "1" );
		expect( result[ 2 ].booleanValue ).toBe( 1 );
		expect( result[ 3 ].booleanValue ).toBe( true );
	});
}

function eeqTestCases() {
	const Filter = new ObjectFilter({ property: "booleanValue", eeq: true });
	const result = Filter.apply( testData );

	test( "returns correct number of elements", () => expect( result.length ).toBe( 2 ) );

	test( "returns items in same order", () => {
		expect( result[ 0 ].index ).toBe( 0 );
		expect( result[ 1 ].index ).toBe( 4 );
	});

	test( "only returns values that match filter criteria", () => {
		expect( result[ 0 ].booleanValue ).toBe( true );
		expect( result[ 1 ].booleanValue ).toBe( true );
	});
}