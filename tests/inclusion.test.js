import { expect, jest, test } from "@jest/globals";
import ObjectFilter from "../src/ObjectFilter.js";

const testData = [
	{ prop: "car", index: 0 },
	{ prop: "book", index: 1 },
	{ prop: "racecar", index: 2 },
	{ prop: "card", index: 3 },
	{ prop: "bookends", index: 4 },
	{ prop: "soda", index: 5 },
	{ prop: [ "car" ], index: 6 },
	{ prop: [ "soda" ], index: 7 }
];

describe( "Includes", incTestCases );
describe( "Does not include", nincTestCases );

function incTestCases(){
	const filter = new ObjectFilter({ property: "prop", inc: "car" });
	const result = filter.apply( testData );

	test( "returns correct number of elements", () => expect( result.length ).toBe( 4 ) );

	test( "returns items in same order", () => {
		expect( result[ 0 ].index ).toBe( 0 );
		expect( result[ 1 ].index ).toBe( 2 );
		expect( result[ 2 ].index ).toBe( 3 );
		expect( result[ 3 ].index ).toBe( 6 );
	});

	test( "only returns values that match filter criteria", () => {
		expect( result[ 0 ].prop ).toBe( "car" );
		expect( result[ 1 ].prop ).toBe( "racecar" );
		expect( result[ 2 ].prop ).toBe( "card" );
		expect(
			JSON.stringify( result[ 3 ].prop )
		).toBe(
			JSON.stringify( testData[ 6 ].prop )
		);
	});
}

function nincTestCases() {
	const filter = new ObjectFilter({ property: "prop", ninc: "car" });
	const result = filter.apply( testData );

	test( "returns correct number of elements", () => expect( result.length ).toBe( 4 ) );

	test( "returns items in same order", () => {
		expect( result[ 0 ].index ).toBe( 1 );
		expect( result[ 1 ].index ).toBe( 4 );
		expect( result[ 2 ].index ).toBe( 5 );
		expect( result[ 3 ].index ).toBe( 7 );
	});

	test( "only returns values that match filter criteria", () => {
		expect( result[ 0 ].prop ).toBe( "book" );
		expect( result[ 1 ].prop ).toBe( "bookends" );
		expect( result[ 2 ].prop ).toBe( "soda" );
		expect( 
			JSON.stringify( result[ 3 ].prop )
		).toBe(
			JSON.stringify( testData[ 7 ].prop ) 
		);
	});
}