import { expect, jest, test } from "@jest/globals";
import ObjectFilter from "../src/ObjectFilter.js";


describe( "Constructor", constructorTestCases );
describe( "Setters", setterTestCases );
describe( "Getters", getterTestCases );
describe( "Resetters", resetterTestCases );
describe( "Private Methods", _apply_methodTestCases );

function constructorTestCases() {

	test( "filter defaults to null", () => {
		const Filter = new ObjectFilter();
		expect( Filter.filter ).toBe( null );
	});

	test( "filter is set in constructor", () => {
		const Filter = new ObjectFilter({ property: "foo", eq: "bar" });

		expect( Filter.filter.property ).toBe( "foo" );
		expect( Filter.filter.eq ).toBe( "bar" );
	});

}

function setterTestCases() {
	test( "setFilter sets filter", () => {
		const Filter = new ObjectFilter();
		expect( Filter.filter ).toBe( null );

		Filter.setFilter({ property: "foo", eq: "bar" });
		expect( Filter.filter.property ).toBe( "foo" );
		expect( Filter.filter.eq ).toBe( "bar" );
	});
}

function getterTestCases() {
	test( "setFilter sets filter", () => {
		const Filter = new ObjectFilter();
		expect( Filter.filter ).toBe( null );

		Filter.setFilter({ property: "foo", eq: "bar" });
		expect( Filter.filter.property ).toBe( "foo" );
		expect( Filter.filter.eq ).toBe( "bar" );

		const filterGotten = Filter.getFilter();
		expect( filterGotten.property ).toBe( "foo" );
		expect( filterGotten.eq ).toBe( "bar" );

	});
}

function resetterTestCases() {
	test( "removeFilter method sets filter to null", () => {
		const Filter = new ObjectFilter();
		expect( Filter.filter ).toBe( null );

		// Set a filter
		Filter.setFilter({ property: "foo", eq: "bar" });
		expect( Filter.filter.property ).toBe( "foo" );
		expect( Filter.filter.eq ).toBe( "bar" );

		// Double check that filter exists
		const filterGotten = Filter.getFilter();
		expect( filterGotten.property ).toBe( "foo" );
		expect( filterGotten.eq ).toBe( "bar" );

		// Remove filter and check
		Filter.removeFilter();
		expect( Filter.filter ).toBe( null );
	});
}

function _apply_methodTestCases() {
	const Filter = new ObjectFilter({ property: "foo", eq: "bar" });
	test( "apply method returns entire array if no filter is given", () => {
		const testData = [
			{ booleanValue: true, index: 0 },
			{ booleanValue: "true", index: 1 },
			{ booleanValue: "1", index: 2 },
			{ booleanValue: 1, index: 3 },
			{ booleanValue: true, index: 4 }
		];
		const Filter = new ObjectFilter();
		const retVal = Filter.apply( testData );

		expect( retVal.length ).toBe( 5 );
	});

	test( "apply method returns array unmodified if no filter is given", () => {
		const testData = [
			{ booleanValue: true, index: 0 },
			{ booleanValue: "true", index: 1 },
			{ booleanValue: "1", index: 2 },
			{ booleanValue: 1, index: 3 },
			{ booleanValue: true, index: 4 }
		];
		const Filter = new ObjectFilter();
		const retVal = Filter.apply( testData );

		expect( retVal.length ).toBe( 5 );
		expect( retVal[ 0 ].booleanValue ).toBe( true );
		expect( retVal[ 0 ].index ).toBe( 0 );
		expect( retVal[ 1 ].booleanValue ).toBe( "true" );
		expect( retVal[ 1 ].index ).toBe( 1 );
		expect( retVal[ 2 ].booleanValue ).toBe( "1" );
		expect( retVal[ 2 ].index ).toBe( 2 );
		expect( retVal[ 3 ].booleanValue ).toBe( 1 );
		expect( retVal[ 3 ].index ).toBe( 3 );
		expect( retVal[ 4 ].booleanValue ).toBe( true );
		expect( retVal[ 4 ].index ).toBe( 4 );
	});

	test( "apply method returns array unmodified if no comparator is provided", () => {
		const testData = [
			{ booleanValue: true, index: 0 },
			{ booleanValue: "true", index: 1 },
			{ booleanValue: "1", index: 2 },
			{ booleanValue: 1, index: 3 },
			{ booleanValue: true, index: 4 }
		];
		const Filter = new ObjectFilter({ property: "foo" });
		const retVal = Filter.apply( testData );

		expect( retVal.length ).toBe( 5 );
		expect( retVal[ 0 ].booleanValue ).toBe( true );
		expect( retVal[ 0 ].index ).toBe( 0 );
		expect( retVal[ 1 ].booleanValue ).toBe( "true" );
		expect( retVal[ 1 ].index ).toBe( 1 );
		expect( retVal[ 2 ].booleanValue ).toBe( "1" );
		expect( retVal[ 2 ].index ).toBe( 2 );
		expect( retVal[ 3 ].booleanValue ).toBe( 1 );
		expect( retVal[ 3 ].index ).toBe( 3 );
		expect( retVal[ 4 ].booleanValue ).toBe( true );
		expect( retVal[ 4 ].index ).toBe( 4 );
	});

	test( "apply method returns array unmodified if an invalid comparator is provided", () => {
		const testData = [
			{ booleanValue: true, index: 0 },
			{ booleanValue: "true", index: 1 },
			{ booleanValue: "1", index: 2 },
			{ booleanValue: 1, index: 3 },
			{ booleanValue: true, index: 4 }
		];
		const Filter = new ObjectFilter({ property: "foo", doesNotExist: true });
		const retVal = Filter.apply( testData );

		expect( retVal.length ).toBe( 5 );
		expect( retVal[ 0 ].booleanValue ).toBe( true );
		expect( retVal[ 0 ].index ).toBe( 0 );
		expect( retVal[ 1 ].booleanValue ).toBe( "true" );
		expect( retVal[ 1 ].index ).toBe( 1 );
		expect( retVal[ 2 ].booleanValue ).toBe( "1" );
		expect( retVal[ 2 ].index ).toBe( 2 );
		expect( retVal[ 3 ].booleanValue ).toBe( 1 );
		expect( retVal[ 3 ].index ).toBe( 3 );
		expect( retVal[ 4 ].booleanValue ).toBe( true );
		expect( retVal[ 4 ].index ).toBe( 4 );
	});
}