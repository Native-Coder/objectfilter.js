import { expect, jest, test } from "@jest/globals";
import ObjectFilter from "../src/ObjectFilter.js";

const testData = [
	{ val: 0, index: 0 },
	{ val: 1, index: 1 },
	{ val: 2, index: 2 }
];

describe( "Greater Than", gtTestCases );
describe( "Greater than or equal to", gteTestCases );

function gtTestCases() {
	const Filter = new ObjectFilter({ property: "val", gt: 0 });
	const result = Filter.apply( testData );

	test( "returns correct number of elements", () => expect( result.length ).toBe( 2 ) );

	test( "returns items in same order", () => {
		expect( result[ 0 ].index ).toBe( 1 );
		expect( result[ 1 ].index ).toBe( 2 );
	});

	test( "only returns values that match filter criteria", () => {
		expect( result[ 0 ].val ).toBe( 1 );
		expect( result[ 1 ].val ).toBe( 2 );
	});
}

function gteTestCases(){
	const Filter = new ObjectFilter({ property: "val", gte: 0 });
	const result = Filter.apply( testData );

	test( "returns correct number of elements", () => expect( result.length ).toBe( 3 ) );

	test( "returns items in same order", () => {
		expect( result[ 0 ].index ).toBe( 0 );
		expect( result[ 1 ].index ).toBe( 1 );
		expect( result[ 2 ].index ).toBe( 2 );
	});

	test( "only returns values that match filter criteria", () => {
		expect( result[ 0 ].val ).toBe( 0 );
		expect( result[ 1 ].val ).toBe( 1 );
		expect( result[ 2 ].val ).toBe( 2 );
	});
}