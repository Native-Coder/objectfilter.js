import { expect, jest, test } from "@jest/globals";
import ObjectFilter from "../src/ObjectFilter.js";

const testData = [
	{ booleanValue: true, index: 0 },
	{ booleanValue: "true", index: 1 },
	{ booleanValue: "1", index: 2 },
	{ booleanValue: 1, index: 3 },
	{ booleanValue: true, index: 4 }
];

describe( "Inequality", neTestCases );
describe( "Exact inequality", neeTestCases );

function neTestCases() {
	const Filter = new ObjectFilter({ property: "booleanValue", ne: true });
	const result = Filter.apply( testData );

	test( "returns correct number of elements", () => expect( result.length ).toBe( 1 ) );

	test( "returns items in same order", () => {
		expect( result[ 0 ].index ).toBe( 1 );
	});

	test( "only returns values that match filter criteria", () => {
		expect( result[ 0 ].booleanValue ).toBe( "true" );
	});
}

function neeTestCases() {
	const Filter = new ObjectFilter({ property: "booleanValue", nee: true });
	const result = Filter.apply( testData );

	test( "returns correct number of elements", () => expect( result.length ).toBe( 3 ) );

	test( "returns items in same order", () => {
		expect( result[ 0 ].index ).toBe( 1 );
		expect( result[ 1 ].index ).toBe( 2 );
		expect( result[ 2 ].index ).toBe( 3 );
	});

	test( "only returns values that match filter criteria", () => {
		expect( result[ 0 ].booleanValue ).toBe( "true" );
		expect( result[ 1 ].booleanValue ).toBe( "1" );
		expect( result[ 2 ].booleanValue ).toBe( 1 );
	});
}