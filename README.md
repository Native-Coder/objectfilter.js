# ObjectFilter.js
ObjectFilterjs is a library used to filter an array of objects and return the results as a new array. It was intended to make remote object array filtering easier by providing a JSON-based syntax to filters. This allows you to easily build the filter parameters in the client side, and send them to your back-end using any JSON-parsable method you prefer. 

# Design Philosophy
- [x] Should provide a single, simple class
- [x] Should define a standard filter notation
- [x] Filters should be nestable
- [x] Zero dependency


# Installation
### Manual
1. Copy `/src/ObjectFilter.js` into your project
2. Import ObjectFilter class `import ObjectFilter from "/whever/you/put/it/ObjectFilter.js";`

# Filter Syntax
All filters (also known as "filter params" to prevent confusion with the Filter object) are included in an outermost object. Please be aware that multiple filters can be nested and combined using combinators like "and" and "or". In this case, it's important to understand that every element in a combinator is considered a single "filter" (or "filter param").

The basic filter syntax is such that 1 filter will have *either*

- exactly 1 "property" field, and exactly 1 "operator" field
  
  OR
- exatly 1 combinator array.

The value of the property field should be the name (key) of the property that you want to filter by.

For the operator field, the key must be the desired operator, and the value must be the value that you want the operator to act upon. See 

## List of Operators
| Operator | Meaning                  | Description                                                        |
| -------- | ------------------------ | ------------------------------------------------------------------ |
| eq       | Equal-to                 | Property must equal this value                                     |
| eeq      | Exactly equal-to         | Property must have this value AND be of the same type              |
| ne       | Not equal-to             | Property must not equal this value                                 |
| nee      | Exactly not equal-to     | Property must not equal this value AND must not have the same type |
| lt       | Less-than                | Property must be less than this value                              |
| lte      | Less-than or equal-to    | Property must be less than this value, or equal to this value      |
| gt       | Greater-than             | roperty must be greater than this value                            |
| gte      | Greater-than or equal-to | Property must be greater than this value, or equal to this value   |
| inc      | Includes                 | Property must include this value                                   |
| ninc     | Does not include         | Property must not include this value                               |

Filters can be combined with "and" and "or" clauses using an array
| Operator | Meaning | Description                                 |
| -------- | ------- | ------------------------------------------- |
| and      | and     | All filters in an "and" array must be true  |
| or       | or      | Only 1 filter in an "or" array must be true |

## A basic single filter
```javascript
// This is a single filter
{property: "this_is_the_propery", eq:"what_the_property_should_equal"}
```
## Multiple filters combined with combinators
>NOTE: Combinators must be arrays!
```javascript
// Both inner filters must be true
{ // This is a single filter
    and:[
        {property: "foo", eq: "bar"},  // This is a single filter
        {property: "fizz", eq: "buzz"} // This is a single filter
    ]
}

// Only 1 of the filters must be true
{// This is a single filter
    or:[
        {property: "foo", eq: "bar"},  // This is a single filter
        {property: "fizz", eq: "buzz"} // This is a single filter
    ]
}
```

## Nesting filters
Filters can be easily nested because every object is considered a "filter"
```javascript
// Finds objects where ((this="that" AND foo="bar") OR fizz="buzz")
{ // Filter Object A
    or:[
        { // Filter Object A.1
            and:[
                    {property: "this", eq: "that"}, //Filter Object A.1.1
                    {property: "foo", eq: "bar"}    //Filter Object A.1.2
                ]
        },
        {property: "fizz", eq: "buzz"} // Filter object A.2
    ]
    
}
```

# Usage
Create an instance of the ObjectFilter class, and pass to it the filter you want (See: Filter Syntax)

### Basic Filter

```javascript
import ObjectFilter from "/some/path/ObjectFilter.js"

let Filter = new ObjectFilter()
const filterParams = {property: "foo", eq:"bar"}

Filter.setFilter(filterParams);

dataArr = [
    {foo: "bar"},
    {foo: "baz"}
]

/**
 * This will only have the objects that have a "foo" property equal to "bar"
 * */
let filteredData = Filter.apply(dataArr);
```

## Combinators in-depth
There are 2 combinators defined in the filter syntax. Those are the "and" combinator, and the "or" combinator. "And" and "Or" are special because they *must* be arrays.

#### "and" combinator

```javascript
import ObjectFilter from "../src/ObjectFilter.js";

const testData = [
	{ foo: "bar", fizz: "buzz", index: 0 },
	{ foo: "baz", fizz: "burr", index: 1 },
    { foo: "bar", fizz: "buzz", index: 2 },
];

// Find all objects where foo = "bar" and fizz = "buzz"
let Filter = new ObjectFilter({ 
    and: [
        { property: "foo", eq: "bar" },
        { property: "fizz", eq: "buzz" }
    ]   
});

let result = Filter.apply( testData );
```

The above example will filter the testData object to find only the objects where foo = "bar" AND fizz = "buzz. The resulting array will be as follows

```javascript
const testData = [
	{ foo: "bar", fizz: "buzz", index: 0 },
    { foo: "bar", fizz: "buzz", index: 2 },
];
```
Notice that the filter removed the object `{ foo: "baz", fizz: "burr", index: 1 }` because it did not match both criteria

#### "or" combinator

```javascript
import ObjectFilter from "../src/ObjectFilter.js";

const testData = [
	{ foo: "bar", fizz: "burr", index: 0 },
	{ foo: "baz", fizz: "burr", index: 1 },
    { foo: "foo", fizz: "buzz", index: 2 },
];

// Find all objects where foo = "bar" and fizz = "buzz"
let Filter = new ObjectFilter({ 
    or: [
        { property: "foo", eq: "bar" },
        { property: "fizz", eq: "buzz" }
    ]   
});

let result = Filter.apply( testData );
```

The above example will filter the testData object to find only the objects where foo = "bar" OR fizz = "buzz. The resulting array will be as follows

```javascript
const testData = [
	{ foo: "bar", fizz: "burr", index: 0 },
    { foo: "foo", fizz: "buzz", index: 2 },
];
```
Notice that the filter removed the object `{ foo: "baz", fizz: "burr", index: 1 }` because it did not match either criteria

## Nested Properties
You can filter based on the value of a nested property by using dot syntax as the value of the property field.

example
```javascript
const myObjectArray = [
    {
        name: "Abraham Lincoln",
        address: {
            street: "123 Washington Way",
            state: "DC",
            zip: "1234"
        }
    },{
        name: "Santa Claus",
        address: {
            street: "345 North-Pole drive",
            state: "North-Pole",
            zip: "5678"
        }
    },{
        name: "Ronald Reagan",
        address: {
            street: "456 Presidential Ave.",
            state: "DC",
            zip: "1234"
        }
    }
]


// Returns all objects where address.state equals "DC"
const Filter = new ObjectFilter({
    property: "address.state", eq: "DC"
});

const result = Filter.apply(myObjectArray);
```

## Reusing a single FilterObject Instance
If you prefer, you can re-use the FilterObject instance by calling the "apply" method with an optional second filter paramter

example:
```javascript
import ObjectFilter from "../src/ObjectFilter.js";

const testData = [
	{ foo: "bar", fizz: "buzz", index: 0 },
	{ foo: "baz", fizz: "burr", index: 1 },
    { foo: "bar", fizz: "buzz", index: 2 },
];

// Do not set persistent filter params
const Filter = new ObjectFilter();

// Find all objects where foo = "bar" and fizz = "buzz"
let filterParams = { 
    and: [
        { property: "foo", eq: "bar" },
        { property: "fizz", eq: "buzz" }
    ]   
};

let result = Filter.apply( testData, filterParams );
```

Notice that we did not provide the filter params to the constructor, which means the "Filter" instance does not have a persistent filter set. You must pass a new set of filterParams every time you call "apply". You can set the persistent filter after instantiating with the "set" method

## The `.setFilter()` method
The set method is used to set the persisTent filterParams after object instantiation. After `.setFilter(filterParams)` is called on your FilterObject instance, you can repeatedly call `.apply(arr)` and the filterParams you set will apply without having to specify them again

```javascript
import ObjectFilter from "../src/ObjectFilter.js";

const testData = [
	{ foo: "bar", fizz: "buzz", index: 0 },
	{ foo: "baz", fizz: "burr", index: 1 },
    { foo: "bar", fizz: "buzz", index: 2 },
];


// Find all objects where foo = "bar" and fizz = "buzz"
let filterParams = { 
    and: [
        { property: "foo", eq: "bar" },
        { property: "fizz", eq: "buzz" }
    ]   
};

// You can set the persistent filterParms after instantiation with the "set method
const Filter = new ObjectFilter();
// Makes the provided filter params persistent
Filter.setFilter( filterParams );

// filterParams will always be used by ".apply(arr)" after ".set(filterParams) is called
let result = Filter.apply( testData );
```

## The `.getFilter()` method
This simply returns the filterParams that are currently set persistently. 

```javascript
import ObjectFilter from "../src/ObjectFilter.js";

let filterParams = { 
    and: [
        { property: "foo", eq: "bar" },
        { property: "fizz", eq: "buzz" }
    ]   
};

// You can set the persistent filterParms after instantiation with the "set method
const Filter = new ObjectFilter(filterParams);

/** Outputs
    { 
        and: [
            { property: "foo", eq: "bar" },
            { property: "fizz", eq: "buzz" }
        ]   
    }
 */
console.log(Filter.getFilter());
```

## The `.removeFilter()` method
The `.removeFilter()` method simply removes any persistent filter params that may have been set

```javascript
import ObjectFilter from "../src/ObjectFilter.js";

const testData = [
	{ foo: "bar", fizz: "buzz", index: 0 },
	{ foo: "baz", fizz: "burr", index: 1 },
    { foo: "bar", fizz: "buzz", index: 2 },
];

let filterParams = { 
    and: [
        { property: "foo", eq: "bar" },
        { property: "fizz", eq: "buzz" }
    ]   
};

// You can set the persistent filterParms after instantiation with the "set method
const Filter = new ObjectFilter(filterParams);

Filter.removeFilter();
Filter.apply(testData); // Will filter nothing because the persistent filter params were removed
Filter.apply(testData, filterParams) // Will use the provided filterParams, but not make them persistent
```

## How persistent filterParams work with the `.apply()` method
Even if you have persistent filter params set, whether through the constructor, or the `.setFilter()` method, the `.apply()` method will *always* use any filter params you manually pass to it, and it will *never* make those filter params persistent. This lets you have a set of persistent filter params, but also be able to pass in custom filter params to `.apply()` for one-off uses

```javascript
import ObjectFilter from "../src/ObjectFilter.js";

const testData = [
	{ foo: "bar", fizz: "buzz", index: 0 },
	{ foo: "baz", fizz: "burr", index: 1 },
    { foo: "bar", fizz: "buzz", index: 2 },
];

let filterParams1 = { property: "foo", eq: "bar" }
let filterParams2 = { property: "fizz", eq: "buzz"}

// filterParams1 will now be persistent
const Filter = new ObjectFilter(filterParams1);

// Will use filterParams1
Filter.apply(testData);
// Will use filterParams2, but filterParams1 will remain set as persistent
Filter.apply(testData, filterParams2)
// Will once again use filterParams1, because calling .apply() with a different set of params does not make that set persistent
Filter.apply(testData); 
```

# Additional Options

### What if the key of my property contains a period?
If the key of the property that you are filtering contains a period, you can simply add the `literalPropKey` attribute to the filter and set it to true. This will tell the ObjectFilter class to treat your object key literally instead of parsing it as a nested property.

```javascript
	const testData = [
		{ "foo.bar": "baz", fizz: "buzz", index: 0 },
		{ "foo.bar": "bar", fizz: "buzz", index: 1 }
	];

    // Will treat "foo.bar" as a literal key instead of
    // assuming it is nested
	const Filter = new ObjectFilter({
        property: "foo.bar",
        eq: "baz",
        literalPropKey: true
    });

    // result will contain the the same value as testData[0]
	const result = Filter.apply( testData );
```