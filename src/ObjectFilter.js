/**
 * Represents a single data filter. Must include a property name which exactly
 * matches the property of the objects that you want the filter to operate on.
 * Must include exactly one operator. Combinators are optional and support
 * filter nesting.
 * @typepdef {Object} FilterObject
 * @property {string} propperty - The name of the pproperty to operate on
 * @property {string} [groupBy] - If included, the filtered results will be grouped by the given property
 * @property {string} [rollup] - (NOT YET IMPLEMENTED) If included, the filtered results will be rolled up based on the provided property
 * @property {(number|string)} eq - "equals" operator
 * @property {(number|string)} eeq - "exactly equals" operator
 * @property {(number|string)} ne - "not equals" operator
 * @property {(number|string)} nee - "exactly not equals" operator
 * @property {(number|string)} lt - "less-than" operator
 * @property {(number|string)} lte - "less-than or equal-to" operator
 * @property {(number|string)} gt - "greater-than" operator
 * @property {(number|string)} gte - "greater-than or equal-to" operator
 * @property {(number|string)} inc - "includes" operator
 * @property {(number|string)} ninc - "does not include" operator
 * @property {(number|string)} exc - (REMOVED - cannot think of use case) "exclude" operator
 * @property {FilterObject[]} [and] - "and" combinator: An array of FilterObjects which all must be true for this one to be true
 * @property {FilterObject[]} [or] - "or" combinator: An array of FilterObjects which only one must be true for this one to be true
 * 
 */

/**
 * Class representing a filter. It is cappable of filtering an array of
 * objects based on specied operators, properties, and values
 */
export default class ObjectFilter {

	/**
     * @constructor
     * @param {FilterObject} filter - The filter that will be applied when the `apply` method is called
     */
	constructor( filter = null ) {
		this.filter = filter ?? null;
	}

	/**
     * Sets the filter
     * @param {FilterObject} filter - The filter that will be applied when the `apply` method is called
     */u;
	setFilter( filter ) {
		this.filter = filter;
	}

	/**
     * Returns the currently set filter
     * @returns FilterObject
     */
	getFilter() {
		return this.filter;
	}

	/**
     * Sets the current filter to null
     */
	removeFilter() {
		this.filter = null;
	}

	/**
     * Applies the provided filter, or the set filter, to the given array of objects
     * @param {Object[]} dataArr - An array of objects
     * @param {FilterObject} [filter] - The FilterObject to use on the provided data set. If no FilterObject is provided,
     * then the previously set filter will be used. Filters can be set in the constructor, or using the `set` method
     * @returns {Object[]} - The filtered array of objects
     */
	apply( dataArr, filter = this.filter ) {
		if ( !filter ) return dataArr;
		let results = dataArr.filter( d => this._test( d, filter ) );
		return results;
	}

	/**
     * Tests a single object against the provided filter
     * @inner
     * @param {Object} datum - A single object to test the filter against
     * @param {FilterObject} filter - The filter to use to test the provided datum
     * @returns {boolean} - true if the filter is matches. False if it is not
     */
	_test( datum, filter ) {

		try {

			if ( Array.isArray( filter.and ) ) {
				for ( let i = 0; i < filter.and.length; i++ ) {
					if ( !this._test( datum, filter.and[ i ] ) )
						return false;
				}
				return true;
			} else if ( Array.isArray( filter.or ) ) {
				for ( let i = 0; i < filter.or.length; i++ ) {
					if ( this._test( datum, filter.or[ i ] ) )
						return true;
				}
				return false;
			}

			let property = filter.property;
			let compareValue = datum[ property ];

			// Allows for testing of nested properties.
			if ( property.includes( "." ) && !filter.literalPropKey ) {
				let filterParts = property.split( "." );
				compareValue = datum;
				// Traverse the object property tree to get to the actual value
				for ( let i = 0; i < filterParts.length; i++ )
					compareValue = compareValue[ filterParts[ i ] ];
			}

			/**
             * Here we use if( "property" in Object ) to overcome the 
             * limitations of hasOwnProperty and typeof.
             * @see: https://stackoverflow.com/questions/26461135/javascript-hasownproperty-vs-typeof
             */
			if ( "eq" in filter )
				return compareValue == filter.eq;
			else if ( "eeq" in filter )
				return compareValue === filter.eeq;
			else if ( "ne" in filter )
				return compareValue != filter.ne;
			else if ( "nee" in filter )
				return compareValue !== filter.nee;
			else if ( "gt" in filter )
				return compareValue > filter.gt;
			else if ( "gte" in filter )
				return compareValue >= filter.gte;
			else if ( "lt" in filter )
				return compareValue < filter.lt;
			else if ( "lte" in filter )
				return compareValue <= filter.lte;
			else if ( "inc" in filter )
				return compareValue.includes( filter.inc );
			else if ( "ninc" in filter )
				return !compareValue.includes( filter.ninc );
			// else if ( "exc" in filter )
			// 	return !( typeof datum[ filter.property ] in filter ) ^ filter.exc;
		} catch ( e ) {
			return false;
		}
		// If no valid operator was found, assume that the datum has passed
		return true;
	}
}